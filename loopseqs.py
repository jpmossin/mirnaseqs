from sys import stderr
import conf
from mirna import readHairpins, readMiRNARecords


def computeLoopSeq(hairpin, matures):
    """
    Find the loop-section of the hairpin's sequence
    """
    index1 = hairpin.seq.index(matures[0].seq)
    index2 = hairpin.seq.index(matures[1].seq)
    p5, p3 = (matures[0], matures[1]) if index1 < index2 else (matures[1], matures[0])
    loopFrom, loopTo = (hairpin.seq.index(p5.seq) + len(p5.seq), hairpin.seq.index(p3.seq))
    return hairpin.seq[loopFrom:loopTo]


def computeLoopSeqs(miRNAs, hairpin_map):
    """
    Try to find the loop-sequence for
    all hairpins that have both 3' and 5' miRNAs
    """
    hairpins2Mature = {}
    for miRNA in miRNAs:
        for hairpin in miRNA.hairpins:
            hairpins2Mature.setdefault(hairpin.id, []).append(miRNA)

    onlyOneMature = []
    loopSeqs = {}
    for hairpin_id in hairpins2Mature:
        matures = hairpins2Mature[hairpin_id]
        hairpin = hairpin_map[hairpin_id]
        if len(matures) > 2:
            matures = [mature for mature in matures if "3p" in mature.id or "5p" in mature.id]
        num_matures = len(matures)
        if num_matures == 1:
            onlyOneMature.append(hairpin_id)
        elif num_matures > 2:
            stderr.write("%d mature seqs (%s) found on %s - skipping\n" % (num_matures, [m.id for m in matures], hairpin_id))
        else:
            loopseq = computeLoopSeq(hairpin, matures)
            loopSeqs[hairpin_id] = loopseq
    return loopSeqs, onlyOneMature


def _run():

    hairpins_map = readHairpins(conf.hairpins_file, conf.secondary_file)
    miRNA_map = readMiRNARecords(conf.mature_file, hairpins_map)

    loopSeqs, onlyOneMature = computeLoopSeqs(miRNA_map.values(), hairpins_map)
    for hairpin_id, loopseq in loopSeqs.items():
        print hairpin_id, loopseq
    print "\nOnly one miRNA was found on the following hairpins"
    for hairpin_id in onlyOneMature:
        print hairpin_id

if __name__ == "__main__":
    _run()
