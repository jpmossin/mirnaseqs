

def matureSecondaryBinary(miRNA):
    """
    Compute the secondary structure of the mature miRNA
    (i.e the section of the the hairipin that corresponds to
    the mature sequence) in a "binary" format: 0=no base-pair,
    1=base-pair.
    """
    hairpin = miRNA.hairpins[0]     # todo: what if there are multiple hairpins
    binary = [str(int(c != '.')) for c in hairpin.secondary]
    startPos = hairpin.seq.index(miRNA.seq)
    matureBinary = binary[startPos: startPos + len(miRNA.seq)]
    return "".join(matureBinary)


if __name__ == "__main__":
    print ""
