# Note: an isomir is simply represented by the MiRNA class

import conf
import matureSecondary
from matureSecondary import matureSecondaryBinary
from mirna import MiRNA, findHairpinsForMiRNA, readHairpins

_isomirFile = conf.data_dir + "daub_etal_hsa.csv"

def readIsomiRs(isomirFile, hairpin_map):
    """
     Read isomiRs from a file containing one isomiR per line in the format:
     "miRNA-id\tsequence". The first line of the file is skipped.
    """
    with open(isomirFile) as f:
        linesSplit = [line.strip().split("\t") for line in f.readlines()[1:]]
        isomiRs = [MiRNA(line[0], line[1], []) for line in linesSplit]
        for isomiR in isomiRs:
            isomiR.hairpins = findHairpinsForMiRNA(isomiR, hairpin_map)
        return isomiRs


def _run():
    hairpins_map = readHairpins(conf.hairpins_file, conf.secondary_file)
    isomiRs = readIsomiRs(_isomirFile, hairpins_map)
    nonTemplated = [isomiR for isomiR in isomiRs if not isomiR.hairpins]
    templated = [isomiR for isomiR in isomiRs if isomiR.hairpins]
    for isomiR in templated:
        print "%s\t%s\t%s" % (isomiR.id, isomiR.seq, matureSecondaryBinary(isomiR))
    print "No mathcing haripin found for the following isomiRs:"
    for isomiR in nonTemplated:
        print "%s\t%s" % (isomiR.id, isomiR.seq)
if __name__ == "__main__":
    _run()