import subprocess
from mirna import readHairpins, readMiRNARecords
import conf

def _matchPositions(secondary):
    """
    Positions of matching parenthesis; if the
    parenthesis at position i is matched by the
    parenthesis at position j, then matches[i] = j, matches[j] = i.
    If there is no base-pair at pos i, then matches[i] = None
    """
    stack = []
    matches = [None] * len(secondary)
    for i, c in enumerate(secondary):
        if c == "(":
            stack.append(i)
        elif c == ")":
            j = stack.pop()
            matches[i] = j
            matches[j] = i
    return matches


def _loopstring(hairpinseq, matches, startPos, numSteps=5, p5=True):
    """
    Calculate RNAfold input strings and corresponding constraints,
    as described in Fig 4.1 in [fordypning, Mossin] .
    """
    stepDir = 1 if p5 else -1   # Calculate 3p or 5p energy.
    toPos = startPos + (numSteps * stepDir)
    included = range(startPos, toPos, stepDir)
    i, firstMatch = startPos, None
    while not firstMatch:   # walk until there is a base pair at position i
        firstMatch = i if matches[i] else None
        i += stepDir

    otherEnd = matches[firstMatch] - stepDir * max((numSteps - abs(firstMatch - startPos)), 0)
    included += range(otherEnd + stepDir, otherEnd + stepDir * numSteps + stepDir, stepDir)
    loopStr = "".join([hairpinseq[pos] for pos in included])
    constrStr = "".join(["|" if matches[pos] in included else "." for pos in included])
    loopStr = loopStr[:numSteps] + ("L" * 6) + loopStr[numSteps:]   # add a "loop"
    constrStr = constrStr[:numSteps] + ("." * 6) + constrStr[numSteps:]
    return loopStr, constrStr


def calculate5pStability(miRNA, p5=True):
    """
    Calculate the termini stability of the 5' or 3' end
    of a MiRNA object.
    """
    hairpin = miRNA.hairpins[0]  # todo: what if there are multiple hairpins
    startPos = hairpin.seq.index(miRNA.seq)
    if not p5:
        startPos += len(miRNA.seq) - 1
    matches = _matchPositions(hairpin.secondary)
    loopStr, constrStr = _loopstring(hairpin.seq, matches, startPos, p5=p5)
    return _RNAfoldCall(loopStr, constrStr)


def _RNAfoldCall(loopStr, constrStr, mir=None):
    """
    RNAfold; call and parse output
    """
    proc = subprocess.Popen("RNAfold -C", shell=True, stdout=subprocess.PIPE, stdin=subprocess.PIPE,
                            stderr=subprocess.PIPE)
    out, err = proc.communicate(input=loopStr + "\n" + constrStr)
    if err:
        print "%s\n%s\n%s\n%s\n" % (err.strip(), mir, loopStr, constrStr)
        exit(1)
    energy = float(out[out.index(" "):].replace(")", "").replace("(", "").strip())
    return energy


def _run():
    miRNA_map = readMiRNARecords(conf.mature_file, readHairpins(conf.hairpins_file, conf.secondary_file))

    # example: Print energy values for all miRNAs from
    # the local mature miRNA fasta file:
    for miRNA in miRNA_map.values():
        print miRNA.id, calculate5pStability(miRNA)

if __name__ == "__main__":
    _run()