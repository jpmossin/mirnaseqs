from Bio import SeqIO
from sys import stderr


class Hairpin:
    def __init__(self, hairpin_id, seq, secondary):
        self.id = hairpin_id
        self.seq = str(seq)
        self.secondary = secondary

    def __str__(self):
        return "Hairpin(%s)" % self.id


class MiRNA:
    def __init__(self, mature_id, seq, hairpins):
        self.id = mature_id
        self.seq = str(seq)
        self.hairpins = hairpins    # one mature seq can stem from multiple (related) hairpins.

    def __str__(self):
        return "MiRNA(%s)" % self.id


def readHairpins(hairpins_file, secondary_file):
    """
    Read in stemloop sequences from a local fasta file,
    and return a map of: 'hairpin_id -> Hairpin object'
    """
    f = open(hairpins_file)
    secondary_map = _readSecondary(secondary_file)
    stemloop_map = {}
    for record in SeqIO.parse(f, "fasta"):
        stemloop_map[record.id] = Hairpin(record.id, record.seq, secondary_map[record.id])
    return stemloop_map


def _readSecondary(secondary_file):
    """
    Read in secondary info from local fasta file,
    converting it to 'parenthesis style'.
    Return a map of: 'miRNA_id -> secondary structure'.
    The structure is given in 'parenthesis style', see below
    """

    def rnafoldStyle(data):
        """
        Convert from mirbase secondary structure format:
                 c   -  u            c  c    gagc
        ccuauguag ggc ca caaaguggaggc cu ucuu    c
        ||||||||| ||| || |||||||||||| || ||||
        ggguacguc ccg gu guuucaccuucg ga agag    u
                 a   u  -            u  a    uaag
        to:
        (((((((((.(((((.((((((((((((.((.((((..........)))).)).)))))))))))))).))).)))))))))
        """
        res = ""
        nucs = "acgu"
        for top, bot in zip(data[0], data[1][:-1]):
            if top in nucs:
                res += "."
            elif bot in nucs:
                res += "("
        if data[1][-1] in nucs:
            res += "."
        if data[2][-1] in nucs:
            res += "."
        if data[3][-1] in nucs:
            res += "."
        for bot, top in reversed(zip(data[4][:-1], data[3])):
            if bot in nucs:
                res += "."
            elif top in nucs:
                res += ")"
        return res

    f = open(secondary_file)
    lines = [l[:-1].lower() for l in f.readlines()]
    structMap = {}
    for i in range(0, len(lines), 8):
        mid = lines[i].split(" ")[0][1:]
        structMap[mid] = rnafoldStyle(lines[i + 2: i + 7])
    f.close()
    return structMap


def findHairpinsForMiRNA(mature_record, hairpin_map):
    """
    Try to find the hairpins of the mature miRNA (could be more
    than one). Candidate hairpins are found by naming conventions, and
    then checked for actually containing the complete mature
    sequence.
    """
    mature_id = mature_record.id
    hairpin_id = mature_id.lower()
    if mature_id.count("-") > 2:
        hairpin_id = hairpin_id.rsplit("-", 1)[0].strip()   # mmu-miR-123-3p -> mmu-mir-123
    extensions = [""] + ["-%d" % i for i in range(1, 10)] + ["a", "b", "c", "d", "e", "f", "g", "h"]
    matching_hairpins = []
    for ext in extensions:
        hairpin = hairpin_map.get(hairpin_id + ext)
        if hairpin and str(mature_record.seq) in hairpin.seq:
            matching_hairpins.append(hairpin)
    return matching_hairpins


def readMiRNARecords(miR_file, hairpin_map):
    """
    Return a map of: 'mature_id -> MiRNA object'
    Read in mature miRNAs from a local fasta file.
    """
    f = open(miR_file)
    miRNAs = {}
    for record in SeqIO.parse(f, "fasta"):
        hairpins = findHairpinsForMiRNA(record, hairpin_map)
        if hairpins:
            miRNAs[record.id] = MiRNA(record.id, record.seq, hairpins)
        else:
            stderr.write("No hairpins found for %s, skipping\n" % record.id)
    return miRNAs






